/**
* Author: Brandon Jacobsen
* Email: brandj94@vt.edu
* Date Started: 7/29/2015
* Last Update: 8/2/2015
*
**/

/**
* THINGS TO DO:
*
* 1. When the window is resized the application still works correctly
* 2. Load dialog to add different tilesheets to the map editor
* 3. Save Dialog to save map
* 4. Allow the user to specify the size of each tile and how big the map should be
* 5. Fix the way the map is stored (I.E. Stores what tilesheets are used and possibly a symbol table?)
* 6. Display the tiles and grid in a view that way the user can scroll around and see the entire map
* 7. Add ability to add map collision tiles
* 8. Add layer support (Only useful if we add feature to allow map to have varied size tiles)
* 9. Preview Tile Placement ---> FINISHED
* 10. Right click removes tiles from map ---> FINISHED
*
*
* BUGS:
* 
* 1. Tiles can be overlapped in a single grid cell
**/

#pragma once
#include <SFML/Graphics.hpp>
#include <fstream>
#include <iostream>
#include <vector>
#include <algorithm>
#include "tile.h"

int main()
{
    sf::RenderWindow window(sf::VideoMode(1152, 640), "Map Editor");
	std::vector<Tile> placedTiles;
	std::vector<Tile> tiles;
	int tile_array[100] = {};
	Tile tile;
	int idCounter = 1;
	int offsetY = 0;

	sf::Texture texture;
	texture.loadFromFile("res/sprites/tiles.jpg");
	sf::Vector2u size = texture.getSize();

	//Generate a tile set from a tilesheet
	for (int x = 0; x < size.x - 64; x += 64)
	{
		Tile tile;
		tile.setID(idCounter);
		x += 2;
		sf::Sprite sprite;
		sprite.setTexture(texture);
		sprite.setTextureRect(sf::IntRect(x, 2, 64, 64));
		sprite.setPosition(0,offsetY);
		tile.setSprite(sprite);
		offsetY += 66;
		tiles.push_back(tile);
		idCounter++;
	}

    while (window.isOpen())
    {
		sf::Vector2i mousePos = sf::Mouse::getPosition(window);

        sf::Event event;
        while (window.pollEvent(event))
        {
            if (event.type == sf::Event::Closed)
			{
                window.close();
			}

			//Only save the file if the user presses escape
			if ((event.type == sf::Event::KeyPressed) && (event.key.code == sf::Keyboard::Escape))
			{
				std::ofstream file;
				file.open("map1.txt");
				for (int x = 0; x < 100; x++)
				{
					file << tile_array[x] << " ";
				}
				file.close();
				window.close();
			}

			//Preview tile placement
			if (event.type == sf::Event::MouseMoved)
			{
				if (mousePos.x > 512)
				{
					tile.setPosition(sf::Vector2f(mousePos.x / 64 * 64, mousePos.y / 64 * 64));
				}
			}

			//Tile Placement
			if ((event.type == sf::Event::MouseButtonPressed) && (event.mouseButton.button == sf::Mouse::Left))
			{
				int tilePosX = event.mouseButton.x / 64 * 64;

				//Place the tile into a position the user selects
				if (tilePosX > 448)
				{
					tile_array[(event.mouseButton.x - 512) / 64 + ((event.mouseButton.y / 64) * 10)] = tile.getID();
					tile.setPosition(sf::Vector2f(event.mouseButton.x / 64 * 64, event.mouseButton.y / 64 * 64));
					placedTiles.push_back(tile);
				}
				else
				{
					//Select the current tile if the user chooses from the tile set
					for (auto & tilet : tiles)
					{
						sf::Vector2f tilePos = tilet.getPosition();
						sf::FloatRect bounds = tilet.getSize();
						int mouseX = event.mouseButton.x;
						int mouseY = event.mouseButton.y;
						if ((mouseX >= tilePos.x) && (mouseX <= bounds.width) && (mouseY >= tilePos.y) && (mouseY <= (bounds.height + tilePos.y)))
						{
							tile = tilet;
							break;
						}
					}
				}
			}

			//Removing Tile
			if ((event.type == sf::Event::MouseButtonPressed) && (event.mouseButton.button == sf::Mouse::Right))
			{
				int pos = 0; //Position of element in placedTiles vector

				for (auto & tilet : placedTiles)
				{
					sf::Vector2f tilePos = tilet.getPosition();
					if ((event.mouseButton.x / 64 * 64 == tilePos.x) && (event.mouseButton.y / 64 * 64 == tilePos.y))
					{
						tile_array[(event.mouseButton.x - 512) / 64 + ((event.mouseButton.y / 64) * 10)] = 0;
						placedTiles.erase(placedTiles.begin() + pos);
						break;
					}

					pos++;
				}
			}
		}

        window.clear();

		//Draw the tiles already placed
		for (auto & tile : placedTiles)
			tile.Draw(window);

		//Draw the horizontal grid lines
		for (int x = 512; x < 1200; x += 64)
		{
			sf::RectangleShape line;
			line.setPosition(x, 0);
			line.setSize(sf::Vector2f(1, 1200));
			window.draw(line);
		}

		//Draw the vertical grid lines
		for (int y = 0; y < 640; y += 64)
		{
			sf::RectangleShape line;
			line.setPosition(512, y);
			line.setSize(sf::Vector2f(640, 1));
			window.draw(line);
		}

		//Draw the separate tiles to choose from
		for (auto & tile : tiles)
			tile.Draw(window);

		//Draw preview tile
		if (mousePos.x > 512)
		{
			tile.Draw(window);
		}

        window.display();
    }

    return 0;
}