/**
* Author: Brandon Jacobsen
* Email: brandj94@vt.edu
* Date Started: 7/29/2015
* Last Update: 8/4/2015
*
**/

#pragma once
#include "tile.h"

Tile::Tile()
{

}

void Tile::Draw(sf::RenderTarget &target)
{
	target.draw(sprite);
}

void Tile::setPosition(sf::Vector2f vector)
{
	sprite.setPosition(vector);
}

void Tile::setSprite(sf::Sprite s)
{
	sprite = s;
}

sf::Vector2f Tile::getPosition()
{
	return sprite.getPosition();
}

sf::FloatRect Tile::getSize()
{
	return sprite.getLocalBounds();
}

void Tile::setID(int num)
{
	id = num;
}

int Tile::getID()
{
	return id;
}