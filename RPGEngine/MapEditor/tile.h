/**
* Author: Brandon Jacobsen
* Email: brandj94@vt.edu
* Date Started: 7/29/2015
* Last Update: 8/4/2015
*
**/

#pragma once
#include <SFML/Graphics.hpp>

class Tile {

private:
	sf::Sprite sprite;
	int id;
public:
	Tile();
	void Draw(sf::RenderTarget &target);
	void setPosition(sf::Vector2f vector);
	sf::Vector2f getPosition();
	void setSprite(sf::Sprite s);
	sf::FloatRect getSize();
	void setID(int tag);
	int getID();
};