#pragma once

#include "SFML/Graphics.hpp"
#include <vector>

class Animation
{
private:
	const sf::Texture* _texture;
	std::vector<sf::IntRect> _frames;
public:
	Animation();

	void addFrame(sf::IntRect rect);
	void setSpriteSheet(const sf::Texture& texture);
	const int getFrameCount() const;
	const sf::Texture* getSpriteSheet() const;
	const sf::IntRect getFrame(int frame) const;
};