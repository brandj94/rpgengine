#pragma once

#include "command.h"

class MoveCommand : public Command
{
public:

	enum Direction
	{
		UP,
		DOWN,
		LEFT,
		RIGHT
	};

	MoveCommand(Direction dir){_direction = dir;}
	~MoveCommand(){}
	void execute(Entity* ent);
private:
	Direction _direction;
};