#pragma once

#include "component.h"
#include "animation.h"
#include "renderable.h"
#include <unordered_map>

class AnimatedSpriteComponent : public Component, public Renderable
{
private:
	std::unordered_map<std::string, Animation*> _animationMap;
	const Animation* _animation;
	int _curFrame;
	double _curTime;
	double _frameTime;
	bool _paused;
	std::string _state;

public:
	AnimatedSpriteComponent();

	void setAnimation(const Animation* animation);
	void setAnimation(const std::string &state);
	void update(double dt);
	void play();
	void pause();
	void setFrame(int frame);
	void render(float x, float y);
	bool isPlaying();
	void mapAnimation(const std::string &state, Animation* anim);
	void setState(const std::string &state);
	std::string getState();
};