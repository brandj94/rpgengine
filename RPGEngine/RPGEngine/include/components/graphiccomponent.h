/**
* Author: Brandon Jacobsen
* Email: brandj94@vt.edu
* Date Started: 8/6/2015
* Last Update: 8/6/2015
*
**/

#pragma once

#include "component.h"
#include "SFML\Graphics.hpp"
#include "components\renderable.h"

class GraphicComponent : public Component, public Renderable
{

private:
	sf::Texture texture;
	sf::RectangleShape rect;
	sf::Sprite sprite;
	sf::Vector2f position;

public:
	GraphicComponent();
	GraphicComponent(const std::string &fname, sf::Vector2f pos, unsigned int dep, bool vis);
	GraphicComponent(sf::RectangleShape shape, sf::Vector2f pos, unsigned int dep, bool vis);
	sf::Sprite *getSprite();
	void render(float x, float y);
};