#pragma once

#include <unordered_map>
#include "command.h"
#include "component.h"
#include "entity.h"

class InputComponent : public Component
{
private:
	std::unordered_map<int, Command*> _bindings;
public:
	InputComponent(){}
	void bind(int key, Command* command);
	void execute(int key, Entity* ent);
};