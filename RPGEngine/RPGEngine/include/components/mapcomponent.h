/**
* Author: Brandon Jacobsen
* Email: brandj94@vt.edu
* Date Started: 8/8/2015
* Last Update: 8/8/2015
*
**/

#pragma once

#include "component.h"

class MapComponent : Component
{
	
private:

public:
	MapComponent();
	void update(){};
	void render(){};
};