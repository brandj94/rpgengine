#pragma once

#include "component.h"

class MovementComponent : public Component
{
private:
	bool _moving;
	int _tileSize;
	int _destinationX;
	int _destinationY;
public:
	MovementComponent(){_moving = false; _tileSize = 32;}
	~MovementComponent(){}
	void setMoving(bool move);
	bool isMoving();
	void moveTowards(int destinationX, int destinationY);
	int getTileSize();
	int getDestinationX();
	int getDestinationY();
};