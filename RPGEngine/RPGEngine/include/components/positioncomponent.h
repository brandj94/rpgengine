/**
* Author: Brandon Jacobsen
* Email: brandj94@vt.edu
* Date Started: 8/6/2015
* Last Update: 8/6/2015
*
**/

#pragma once

#include "component.h"
#include "SFML/Graphics.hpp"

class PositionComponent : public Component
{
	
private:
	sf::Vector2f position;

public:
	PositionComponent(sf::Vector2f pos);
	void setPosition(float x, float y);
	sf::Vector2f getPosition();
};