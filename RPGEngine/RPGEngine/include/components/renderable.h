/**
* Author: Brandon Jacobsen
* Email: brandj94@vt.edu
* Date Started: 8/10/2015
* Last Update: 8/10/2015
*
**/

#pragma once

class Renderable
{
	
protected:
	unsigned int depth;
	bool visible;
public:
	Renderable();
	unsigned int getDepth();
	void setDepth(int dep);
	void setVisible(bool vis);
	bool isVisible();
	virtual void render(float x, float y) = 0;
};