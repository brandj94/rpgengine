/**
* Author: Brandon Jacobsen
* Email: brandj94@vt.edu
* Date Started: 8/8/2015
* Last Update: 8/8/2015
*
**/

#pragma once

#include "component.h"

class StatsComponent : Component
{

private:
	int level;
	int hp;
	int maxHp;
	int mp;
	int maxMp;
	int str;
	int spd;

public:
	StatsComponent();
	void setLevel(int lvl);
	void setHp(int stat);
	void setMaxHp(int stat);
	void setMp(int stat);
	void setMaxMp(int stat);
	void setStr(int stat);
	void setSpd(int stat);
	int getLevel();
	int getHp();
	int getMaxHp();
	int getMp();
	int getMaxMp();
	int getStr();
	int getSpd();

	//temp
	void update(){};
	void render(){};
};