#pragma once

#include "SFML\Graphics.hpp"
#include "components\renderable.h"

class TextComponent : public Renderable
{

private:
	sf::Font font;
	sf::Text text;
public:
	TextComponent(const std::string &fontName, const std::string &str, sf::Vector2f pos, int charSize, sf::Color color, bool vis);
	void setPosition(sf::Vector2f pos);
	void setTextSize(int size);
	void setTextColor(sf::Color color);
	sf::Vector2f getPosition();
	void render(float x, float y);
	//temp
	void update(){};
};