/**
* Author: Brandon Jacobsen
* Email: brandj94@vt.edu
* Date Started: 8/9/2015
* Last Update: 8/9/2015
*
**/

#pragma once

#include "component.h"
#include "entities\tileentity.h"
#include "components\renderable.h"
#include <vector>

class TileMapComponent : Component
{

private:
	int cellSize;
	int rows;
	int columns;
	std::vector<TileEntity> tiles;
public:
	TileMapComponent(int tileSize, int numRows, int numCols);
	//temp
	void update(){};
	void render(){};
};