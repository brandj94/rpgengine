#pragma once

#include "components\graphiccomponent.h"
#include "components\textcomponent.h"
#include "entity.h"

class MenuEntity : public Entity
{

private:
	GraphicComponent *graphComp;
	GraphicComponent *cursor;
	int cursorPos;
	std::vector<TextComponent*> textList;
public:
	MenuEntity();
	void update();
};