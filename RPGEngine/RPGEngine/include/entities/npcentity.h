/**
* Author: Brandon Jacobsen
* Email: brandj94@vt.edu
* Date Started: 8/8/2015
* Last Update: 8/8/2015
*
**/

#pragma once

#include "entities\npcentity.h"
#include "entity.h"

class NPCEntity : public Entity
{

private:
	int id;
public:
	NPCEntity();
	void update();
};