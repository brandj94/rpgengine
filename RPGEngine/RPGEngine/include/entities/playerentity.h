/**
* Author: Brandon Jacobsen
* Email: brandj94@vt.edu
* Date Started: 8/6/2015
* Last Update: 8/6/2015
*
**/

#pragma once

#include "components\graphiccomponent.h"
#include "components\positioncomponent.h"
#include "entity.h"

class PlayerEntity : public Entity
{

private:
	//GraphicComponent *graphComp;
	//PositionComponent *posComp;
	bool canMove;

public:
	PlayerEntity(long num) : Entity(num){};
	PlayerEntity(const std::string &fname, sf::Vector2f pos, unsigned int depth);
	void update();
};