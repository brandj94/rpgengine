/**
* Author: Brandon Jacobsen
* Email: brandj94@vt.edu
* Date Started: 8/9/2015
* Last Update: 8/9/2015
*
**/

#pragma once

#include "entity.h"
#include "components\graphiccomponent.h"

class TileEntity : Entity
{

private:
	int gridX;
	int gridY;
	bool walkable;
	GraphicComponent* graphComp;
public:
	TileEntity(int x, int y, bool walk);
	void setTileTexture(const std::string &fname, sf::Vector2f pos, unsigned int dep, bool vis);
	void update();
	int getGridX();
	int getGridY();
	bool isWalkable();
};