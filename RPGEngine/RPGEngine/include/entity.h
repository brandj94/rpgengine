#pragma once

#include <map>
#include <string>

#include "components\component.h"

class Entity
{
	
private:
	long id;
	std::map<std::string, Component*> _components;

public:
	Entity(long idNum){id = idNum;};
	Entity(){};
	~Entity()
	{
		std::map<std::string, Component*>::iterator itr;
		for (itr = _components.begin(); itr != _components.end(); itr++)
		{
			delete(itr->second);
		}
	}

	//Adds a component of the specified type to the entity
	void addComponent(const std::string &type, Component* comp)
	{
		_components[type] = comp;
	}

	//Returns all of the components that are associated with the entity
	const std::map<std::string, Component*>& getComponents()
	{
		return _components;
	}

	Component* getComponent(const std::string &type)
	{
		return _components[type];
	}

	//Removes the specified type of component from the entity
	void removeComponent(const std::string &type)
	{
		_components.erase(type);
	}
};