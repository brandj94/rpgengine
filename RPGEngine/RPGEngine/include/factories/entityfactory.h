#pragma once

#include "entity.h"

class EntityFactory
{
	
public:
	Entity *Create();
};