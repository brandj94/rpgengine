#pragma once

#include <map>
#include <vector>
#include <string>

#include "entity.h"

class EntityManager
{

private:
	long _counter;
	std::map<std::string, Entity*> _entityList;

public:
	EntityManager(){_counter = 0;};
	~EntityManager();
	Entity* createEntity(const std::string &name);
	Entity* getEntity(const std::string &name);
};