/**
* Author: Brandon Jacobsen
* Email: brandj94@vt.edu
* Date Started: 8/6/2015
* Last Update: 8/6/2015
*
**/

#pragma once

#include <vector>

class EventManager
{
	
private:
	std::vector<int> _events;

public:
	EventManager();
	void addEvent(int code);
	std::vector<int> requestEvents();
	void clear();
};