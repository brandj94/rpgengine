#pragma once

#include "SFML\Graphics.hpp"
#include "components\renderable.h"
#include "managers\eventmanager.h"
#include "managers\entitymanager.h"
#include "managers\resourcemanager.h"
#include "systems\rendersystem.h"
#include "systems\inputsystem.h"
#include "systems\movementsystem.h"
#include <vector>

class GameManager
{

private:
	sf::RenderWindow* _window;
	bool _running;
	EventManager* _eventManager;
	EntityManager* _entityManager;
	ResourceManager* _resourceManager;
	RenderSystem* _renderSystem;
	InputSystem* _inputSystem;
	MovementSystem* _movementSystem;
	std::vector<Renderable*>* _renderList;
	static bool instanceCreated;
	static GameManager* game;

public:
	GameManager();
	static GameManager* Get();
	void start(int x, int y, std::string title);
	void stop();
	void update(double dt);
	void render(double dt);
	bool isRunning();
	sf::RenderWindow* getWindow();
	EventManager* getEventManager();
	EntityManager* getEntityManager();
	ResourceManager* getResourceManager();
	std::vector<Renderable*>* GameManager::getRenderList();
};

GameManager* Game();