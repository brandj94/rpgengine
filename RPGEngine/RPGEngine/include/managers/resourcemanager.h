//The Resource Manager keeps track of all of the resources that are
//requested to be used in the program. It has multiple lists that handle the
//variety of resources that are requested.
//
//When requesting a resource there are two possibilities:
//
//The resource hasn't been loaded yet:
//    -The requested resource will be loaded into memory
//    -A pointer to the resource will be stored into its respective
//     collection for future references
//
//The resource has been loaded:
//    -The resource manager will simply return the resource pointer
//

#pragma once

#include <map>
#include "SFML\Graphics.hpp"

class ResourceManager
{

private:
	std::map<std::string, sf::Texture*> textureList;

public:
	ResourceManager(){};
	sf::Texture* loadTexture(const std::string &src);
};