/**
* Author: Brandon Jacobsen
* Email: brandj94@vt.edu
* Date Started: 8/9/2015
* Last Update: 8/9/2015
*
**/

#pragma once

#include "entity.h"
#include "components\graphiccomponent.h"
#include <vector>

class SceneManager
{

private:
	std::vector<Entity*> entityList;
public:
	SceneManager(){};
	void addEntity(Entity* ent);
	void update();
	void render();
	//temp sort render list by depth
	void sortRenderList(std::vector<Renderable*> *list);
};