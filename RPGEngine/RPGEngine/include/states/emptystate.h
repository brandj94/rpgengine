#pragma once

#include "states\istate.h"

class EmptyState : public IState
{
	void onEnter(){};
	void onExit(){};
	void update(){};
	void render(){};
};