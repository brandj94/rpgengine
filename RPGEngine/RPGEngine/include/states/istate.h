#pragma once

#include "managers\scenemanager.h"

class IState
{

protected:
	SceneManager* sceneManager;

public:
	virtual void onEnter() = 0;
	virtual void onExit() = 0;
	virtual void update() = 0;
	virtual void render() = 0;
};