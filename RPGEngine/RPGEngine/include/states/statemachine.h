#pragma once

#include <map>
#include <string>
#include "states\istate.h"
#include "states\emptystate.h"

class StateMachine
{

private:
	std::map<std::string, IState*> gameStates;
	IState *curState;
public:
	StateMachine(){curState = new EmptyState();};
	void update();
	void render();
	void changeState(const std::string &stateName);
	void addState(const std::string &stateName, IState *state);
};