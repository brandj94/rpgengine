#pragma once

#include "states\istate.h"
#include "entities\playerentity.h"
#include "components\tilemapcomponent.h"

class WorldMapState : public IState
{

private:
	PlayerEntity* player;
	SceneManager* sceneManager;
	GraphicComponent* graphComp;
	TileMapComponent* tileMap;
public:
	WorldMapState();
	void onEnter();
	void onExit();
	void update();
	void render();
};