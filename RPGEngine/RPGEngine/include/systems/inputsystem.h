#pragma once

#include "entity.h"
#include <vector>

class InputSystem
{
private:
	std::vector<Entity*> _entityList;
public:
	InputSystem(){};
	void addEntity(Entity* ent);
	void update();
};