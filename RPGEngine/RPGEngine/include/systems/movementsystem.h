#pragma once

#include "entity.h"
#include <vector>

class MovementSystem
{
private:
	std::vector<Entity *> _entityList;
	bool gotNext;
	int curTileX;
	int curTileY;
	int nextTileX;
	int nextTileY;
	double t;
public:
	MovementSystem(){gotNext = false; t = 0;}
	void addEntity(Entity* ent);
	void update(double dt);
	int lerp(int start, int end, double t);
};