#pragma once

#include "entity.h"
#include <vector>

class RenderSystem
{

private:
	std::vector<Entity*> _entityList;

public:
	RenderSystem(){};
	void addEntity(Entity* ent);
	void render(double dt);
};