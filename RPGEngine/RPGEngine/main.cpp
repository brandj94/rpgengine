//For memory leak testing
#define _CRTDBG_MAP_ALLOC
#include <stdlib.h>
#include <crtdbg.h>
#include <iostream>

#include "managers\gamemanager.h"

bool GameManager::instanceCreated = false;
GameManager* GameManager::game = nullptr;

int main()
{
	GameManager *game = GameManager::Get();
	game->start(800, 800, "Battle Engine");
	sf::Clock clock;
	sf::Time curTime = clock.getElapsedTime();

	while (game->isRunning())
	{
		sf::Time newTime = clock.getElapsedTime();
		double frameTime = newTime.asSeconds() - curTime.asSeconds();
		curTime = clock.getElapsedTime();

		game->update(frameTime);
		game->render(frameTime);
	}

	_CrtDumpMemoryLeaks(); //Dump memory leaks to the ouput window
	return 0;
}