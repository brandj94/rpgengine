#include "animation.h"

Animation::Animation() : _texture(NULL)
{

}

void Animation::addFrame(sf::IntRect rect)
{
	_frames.push_back(rect);
}

void Animation::setSpriteSheet(const sf::Texture& texture)
{
	_texture = &texture;
}

const sf::Texture* Animation::getSpriteSheet() const
{
	return _texture;
}

const sf::IntRect Animation::getFrame(int frame) const
{
	return _frames[frame];
}

const int Animation::getFrameCount() const
{
	return _frames.size();
}