#include "commands\movecommand.h"
#include "components\positioncomponent.h"
#include "components\movementcomponent.h"
#include "components\animatedspritecomponent.h"
#include "managers\gamemanager.h"

void MoveCommand::execute(Entity* ent)
{
	PositionComponent* pos = (PositionComponent*) ent->getComponent("position");
	MovementComponent* movement = (MovementComponent*) ent->getComponent("movement");
	AnimatedSpriteComponent* anim = (AnimatedSpriteComponent*) ent->getComponent("animated");

	if (!movement->isMoving())
	{
		switch(_direction)
		{
			case LEFT:
				anim->setState("WALK_LEFT");
				movement->moveTowards(pos->getPosition().x - movement->getTileSize(), pos->getPosition().y);
				break;
			case RIGHT:
				anim->setState("WALK_RIGHT");
				movement->moveTowards(pos->getPosition().x + movement->getTileSize(), pos->getPosition().y);
				break;
			case UP:
				anim->setState("WALK_UP");
				movement->moveTowards(pos->getPosition().x, pos->getPosition().y - movement->getTileSize());
				break;
			case DOWN:
				anim->setState("WALK_DOWN");
				movement->moveTowards(pos->getPosition().x, pos->getPosition().y + movement->getTileSize());
				break;
		}
	}
}