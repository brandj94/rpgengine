#include "components\animatedspritecomponent.h"
#include "managers\gamemanager.h"
#include "SFML\Graphics.hpp"

AnimatedSpriteComponent::AnimatedSpriteComponent()
{
	_curFrame = 0;
	_curTime = 0;
	_frameTime = 10.0;
	_paused = false;
	_state = "";
}

void AnimatedSpriteComponent::setAnimation(const Animation* animation)
{
	//_curFrame = 0;
	_animation = animation;
}

void AnimatedSpriteComponent::setAnimation(const std::string &state)
{
	//_curFrame = 0;
	_animation = _animationMap[state];
}

void AnimatedSpriteComponent::update(double dt)
{

	if (!_paused)
	{
		_curTime += dt;
		if (_curTime > _frameTime / 60)
		{
			if (_curFrame < _animation->getFrameCount() - 1)
			{
				_curFrame++;
			}
			else
			{
				_curFrame = 0;
			}

			_curTime = 0;
		}
	}
}

void AnimatedSpriteComponent::render(float x, float y)
{
	sf::Sprite sprite;
	sprite.setPosition(x, y);
	sprite.setTexture(*(_animation->getSpriteSheet()));
	sprite.setTextureRect(_animation->getFrame(_curFrame));
	if (visible)
	{
		Game()->getWindow()->draw(sprite);
	}
}

void AnimatedSpriteComponent::play()
{
	_paused = false;
}

void AnimatedSpriteComponent::pause()
{
	_paused = true;
}

void AnimatedSpriteComponent::setFrame(int frame)
{
	_curFrame = frame;
}

bool AnimatedSpriteComponent::isPlaying()
{
	return !_paused;
}

void AnimatedSpriteComponent::mapAnimation(const std::string &state, Animation* anim)
{
	_animationMap[state] = anim;
}

void AnimatedSpriteComponent::setState(const std::string &state)
{
	_state = state;
}

std::string AnimatedSpriteComponent::getState()
{
	return _state;
}