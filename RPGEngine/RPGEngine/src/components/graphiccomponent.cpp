/**
* Author: Brandon Jacobsen
* Email: brandj94@vt.edu
* Date Started: 8/6/2015
* Last Update: 8/6/2015
*
**/

#include "components\graphiccomponent.h"
#include "managers\gamemanager.h"

GraphicComponent::GraphicComponent(const std::string &fname, sf::Vector2f pos, unsigned int dep, bool vis)
{
	/*if (fname != "")
	{
		texture.loadFromFile(fname);
		sprite.setTexture(texture);
	}*/
	Game()->getRenderList()->push_back(this);
	texture = *(Game()->getResourceManager()->loadTexture(fname));
	sprite.setTexture(texture);
	sprite.setPosition(pos);
	position = pos;
	depth = dep;
	visible = vis;
}

GraphicComponent::GraphicComponent(sf::RectangleShape shape, sf::Vector2f pos, unsigned int dep, bool vis)
{
	rect = shape;
	position = pos;
	rect.setPosition(pos);
	depth = dep;
	visible = vis;
}

sf::Sprite* GraphicComponent::getSprite()
{
	return &sprite;
}

void GraphicComponent::render(float x, float y)
{
	position = sf::Vector2f(x, y);
	sprite.setPosition(position);
	if (visible)
	{
		if (sprite.getTexture() != NULL)
			Game()->getWindow()->draw(sprite);
		else
			Game()->getWindow()->draw(rect);
	}
}