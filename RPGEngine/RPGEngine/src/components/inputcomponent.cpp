#include "components\inputcomponent.h"

void InputComponent::bind(int key, Command* command)
{
	_bindings[key] = command;
}

void InputComponent::execute(int key, Entity* ent)
{
	if (_bindings[key])
		_bindings[key]->execute(ent);
}