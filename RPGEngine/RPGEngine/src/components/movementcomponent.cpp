#include "components\movementcomponent.h"

void MovementComponent::setMoving(bool move)
{
	_moving = move;
}

bool MovementComponent::isMoving()
{
	return _moving;
}

void MovementComponent::moveTowards(int destinationX, int destinationY)
{
	_destinationX = destinationX;
	_destinationY = destinationY;
	_moving = true;
}

int MovementComponent::getTileSize()
{
	return _tileSize;
}

int MovementComponent::getDestinationX()
{
	return _destinationX;
}

int MovementComponent::getDestinationY()
{
	return _destinationY;
}