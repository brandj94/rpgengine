/**
* Author: Brandon Jacobsen
* Email: brandj94@vt.edu
* Date Started: 8/6/2015
* Last Update: 8/6/2015
*
**/

#include "components/positioncomponent.h"

PositionComponent::PositionComponent(sf::Vector2f pos)
{
	position = pos;
}

void PositionComponent::setPosition(float x, float y)
{
	position = sf::Vector2f(x, y);
}

sf::Vector2f PositionComponent::getPosition()
{
	return position;
}