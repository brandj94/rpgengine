/**
* Author: Brandon Jacobsen
* Email: brandj94@vt.edu
* Date Started: 8/10/2015
* Last Update: 8/10/2015
*
**/

#include "components\renderable.h"
#include "managers\gamemanager.h"

Renderable::Renderable()
{
	//Game()->getComponentManager()->addRenderable(this);
}

void Renderable::setDepth(int dep)
{
	depth = dep;
}

unsigned int Renderable::getDepth()
{
	return depth;
}

void Renderable::setVisible(bool vis)
{
	visible = vis;
}

bool Renderable::isVisible()
{
	return visible;
}