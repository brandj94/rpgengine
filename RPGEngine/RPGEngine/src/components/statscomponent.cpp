/**
* Author: Brandon Jacobsen
* Email: brandj94@vt.edu
* Date Started: 8/8/2015
* Last Update: 8/8/2015
*
**/

#include "components\statscomponent.h"

StatsComponent::StatsComponent()
{
	level = 1;
	hp = 10;
	maxHp = hp;
	mp = 5;
	maxMp = 5;
	str = 10;
	spd = 10;
}

void StatsComponent::setLevel(int lvl)
{
	level = lvl;
}

void StatsComponent::setHp(int stat)
{
	hp = stat;
}

void StatsComponent::setMaxHp(int stat)
{
	maxHp = stat;
}

void StatsComponent::setMp(int stat)
{
	mp = stat;
}

void StatsComponent::setMaxMp(int stat)
{
	maxMp = stat;
}

void StatsComponent::setStr(int stat)
{
	str = stat;
}

void StatsComponent::setSpd(int stat)
{
	spd = stat;
}

int StatsComponent::getLevel()
{
	return level;
}

int StatsComponent::getHp()
{
	return hp;
}

int StatsComponent::getMaxHp()
{
	return maxHp;
}

int StatsComponent::getMp()
{
	return mp;
}

int StatsComponent::getMaxMp()
{
	return maxMp;
}

int StatsComponent::getStr()
{
	return str;
}

int StatsComponent::getSpd()
{
	return spd;
}