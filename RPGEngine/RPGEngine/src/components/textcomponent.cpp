/**
* Author: Brandon Jacobsen
* Email: brandj94@vt.edu
* Date Started: 8/10/2015
* Last Update: 8/11/2015
*
**/

#include "components\textcomponent.h"
#include "managers\gamemanager.h"

TextComponent::TextComponent(const std::string &fontName, const std::string &str, sf::Vector2f pos, int charSize, sf::Color color, bool vis)
{
	font.loadFromFile(fontName);
	text.setFont(font);
	text.setString(str);
	text.setCharacterSize(charSize);
	text.setColor(color);
	text.setPosition(pos);
	visible = vis;
	depth = 0;
}

void TextComponent::setPosition(sf::Vector2f pos)
{
	text.setPosition(pos);
}

void TextComponent::setTextSize(int size)
{
	text.setCharacterSize(size);
}

void TextComponent::setTextColor(sf::Color color)
{
	text.setColor(color);
}

sf::Vector2f TextComponent::getPosition()
{
	return text.getPosition();
}

void TextComponent::render(float x, float y)
{
	if (visible)
		Game()->getWindow()->draw(text);
}