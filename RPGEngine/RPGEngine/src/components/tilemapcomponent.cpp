/**
* Author: Brandon Jacobsen
* Email: brandj94@vt.edu
* Date Started: 8/9/2015
* Last Update: 8/9/2015
*
**/

#include "components\tilemapcomponent.h"

TileMapComponent::TileMapComponent(int tileSize, int numRows, int numCols)
{
	cellSize = tileSize;
	rows = numRows;
	columns = numCols;

	for (int i = 0; i < rows; i++)
	{
		for (int j = 0; j < columns; j++)
		{
			TileEntity tile(i, j, false);
			tile.setTileTexture("res/grasstile.jpg", sf::Vector2f(i * cellSize, j * cellSize), 100, true);
			tiles.push_back(tile);
		}
	}
}