#include "entities\menuentity.h"
#include "managers\gamemanager.h"

MenuEntity::MenuEntity()
{
	//Setup generic menu window
	sf::RectangleShape rect;
	rect.setPosition(0, 0);
	rect.setSize(sf::Vector2f(200, 600));
	rect.setFillColor(sf::Color::White);
	graphComp = new GraphicComponent(rect, sf::Vector2f(600, 100), 0, false);

	//Setup text list
	textList.push_back(new TextComponent("arial.ttf", "Item", sf::Vector2f(675, 125), 24, sf::Color::Black, false));
	textList.push_back(new TextComponent("arial.ttf", "Status", sf::Vector2f(675, 165), 24, sf::Color::Black, false));
	
	//Setup cursor
	cursor = new GraphicComponent("res/selector.png", sf::Vector2f(750, 750), 0, true);
	cursor->setVisible(false);
	cursorPos = 0;

	//Add to the world (Add this to the entity base class you dipshit)
}

void MenuEntity::update()
{
	//Check if user pressed the menu key
	std::vector<int> request = Game()->getEventManager()->requestEvents();
	if (request.size() != 0)
	{
		if (graphComp->isVisible())
		{
			for (auto & text : textList)
				text->setVisible(false);
			cursor->setVisible(false);
			graphComp->setVisible(false);
		}
		else
		{
			for (auto & text : textList)
				text->setVisible(true);
			cursor->setVisible(true);
			graphComp->setVisible(true);
		}
	}

	//Check if user attempted to move cursor (only if menu is open)
	if (graphComp->isVisible())
	{
		/*request = Game()->getEventManager()->requestKeys(sf::Event::KeyPressed, sf::Keyboard::Down);
		if (request.size() != 0)
		{
			if (cursorPos != textList.size() - 1)
				cursorPos++;
			else
				cursorPos = 0;
		}

		request = Game()->getEventManager()->requestKeys(sf::Event::KeyPressed, sf::Keyboard::Up);
		if (request.size() != 0)
		{
			if (cursorPos != 0)
				cursorPos--;
			else
				cursorPos = textList.size() - 1;
		}*/
	}

	//Update the cursor
	//cursor->setPosition(textList[cursorPos]->getPosition().x - 35, textList[cursorPos]->getPosition().y + 5);
}