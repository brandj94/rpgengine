/**
* Author: Brandon Jacobsen
* Email: brandj94@vt.edu
* Date Started: 8/9/2015
* Last Update: 8/9/2015
*
**/

#include "entities\tileentity.h"

TileEntity::TileEntity(int x, int y, bool walk)
{
	gridX = x;
	gridY = y;
	walkable = walk;
}

void TileEntity::setTileTexture(const std::string &fname, sf::Vector2f pos, unsigned int dep, bool vis)
{
	graphComp = new GraphicComponent(fname, pos, dep, vis);
}

void TileEntity::update()
{

}

int TileEntity::getGridX()
{
	return gridX;
}

int TileEntity::getGridY()
{
	return gridY;
}

bool TileEntity::isWalkable()
{
	return walkable;
}