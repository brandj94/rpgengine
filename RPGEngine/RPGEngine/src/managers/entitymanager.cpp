#include "managers\entitymanager.h"

Entity* EntityManager::createEntity(const std::string &name)
{
	Entity* ent = new Entity(_counter);
	_entityList[name] = ent;

	_counter++;

	return ent;
}

Entity* EntityManager::getEntity(const std::string &name)
{
	return _entityList[name];
}

EntityManager::~EntityManager()
{
	std::map<std::string, Entity*>::iterator itr;

	for (itr = _entityList.begin(); itr != _entityList.end(); itr++)
	{
		delete itr->second;
	}
}