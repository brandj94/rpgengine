/**
* Author: Brandon Jacobsen
* Email: brandj94@vt.edu
* Date Started: 8/6/2015
* Last Update: 8/6/2015
*
**/

#include "managers\eventmanager.h"

EventManager::EventManager()
{

}

void EventManager::addEvent(int code)
{
	_events.push_back(code);
}

std::vector<int> EventManager::requestEvents()
{
	return _events;
}

void EventManager::clear()
{
	_events.clear();
}