#include "managers\gamemanager.h"
#include "components\graphiccomponent.h"
#include "components\positioncomponent.h"
#include "components\animatedspritecomponent.h"
#include "components\inputcomponent.h"
#include "components\movementcomponent.h"
#include "animation.h"
#include "commands\movecommand.h"

GameManager::GameManager()
{
	_running = true;
}

bool GameManager::isRunning()
{
	return _running;
}

sf::RenderWindow* GameManager::getWindow()
{
	return _window;
}

void GameManager::update(double dt)
{
	if (_running)
	{
		sf::Event event;

		while (_window->pollEvent(event))
		{
			if (event.type == sf::Event::Closed)
			{
				game->stop();
                _window->close();
			}

			if ((event.type == sf::Event::KeyPressed) && (event.key.code == sf::Keyboard::Escape))
			{
				game->stop();
				_window->close();
			}

			//Temporary input handling until I implement a input component and event handler
			/*if ((event.type ==  sf::Event::KeyPressed) && (event.key.code == sf::Keyboard::W))
			{
				PositionComponent* pos = (PositionComponent*)_entityManager->getEntity("player")->getComponent("position");
				pos->setPosition(pos->getPosition().x, pos->getPosition().y - 10);
			}

			if ((event.type ==  sf::Event::KeyPressed) && (event.key.code == sf::Keyboard::D))
			{
				PositionComponent* pos = (PositionComponent*)_entityManager->getEntity("player")->getComponent("position");
				pos->setPosition(pos->getPosition().x + 10, pos->getPosition().y);
			}

			if ((event.type ==  sf::Event::KeyPressed) && (event.key.code == sf::Keyboard::A))
			{
				PositionComponent* pos = (PositionComponent*)_entityManager->getEntity("player")->getComponent("position");
				pos->setPosition(pos->getPosition().x - 10, pos->getPosition().y);
			}

			if ((event.type ==  sf::Event::KeyPressed) && (event.key.code == sf::Keyboard::S))
			{
				PositionComponent* pos = (PositionComponent*)_entityManager->getEntity("player")->getComponent("position");
				pos->setPosition(pos->getPosition().x, pos->getPosition().y + 10);
			}*/

			if (event.type == sf::Event::KeyPressed)
			{
				getEventManager()->addEvent(event.key.code);
			}
		}

		_inputSystem->update();
		_movementSystem->update(dt);
		getEventManager()->clear();
	}
}

void GameManager::render(double dt)
{
	if (_running)
	{
		_window->clear();

		/*for (Renderable* renderable : *_renderList)
		{
			renderable->render();
		}*/

		_renderSystem->render(dt);

		_window->display();
	}
}

GameManager* GameManager::Get()
{
	if (!instanceCreated)
	{
		game = new GameManager();
		instanceCreated = true;
		return game;
	}
	else
		return game;
}

void GameManager::start(int x, int y, std::string title)
{
	_window = new sf::RenderWindow(sf::VideoMode(x, y), title);

	_eventManager = new EventManager();
	_entityManager = new EntityManager();
	_resourceManager = new ResourceManager();
	_renderSystem = new RenderSystem();
	_inputSystem = new InputSystem();
	_movementSystem = new MovementSystem();
	_renderList = new std::vector<Renderable*>;

	Entity* ent = _entityManager->createEntity("player");
	//ent->addComponent("renderable", new GraphicComponent("res/player.png", sf::Vector2f(0, 0), 0, true));
	ent->addComponent("position", new PositionComponent(sf::Vector2f(200, 200)));

	//TESTING MOVEMENT
	ent->addComponent("movement", new MovementComponent());
	//END TESTING MOVEMENT

	//TESTING INPUT
	InputComponent* inputComp = new InputComponent();
	inputComp->bind(sf::Keyboard::A, new MoveCommand(MoveCommand::Direction::LEFT));
	inputComp->bind(sf::Keyboard::D, new MoveCommand(MoveCommand::Direction::RIGHT));
	inputComp->bind(sf::Keyboard::W, new MoveCommand(MoveCommand::Direction::UP));
	inputComp->bind(sf::Keyboard::S, new MoveCommand(MoveCommand::Direction::DOWN));
	ent->addComponent("input", inputComp);
	//END TESTING INPUT

	//TESTING ANIMATION
	sf::Texture* spriteSheet = (Game()->getResourceManager()->loadTexture("res/bomberman.png"));
	//Down Walking Animation
	Animation* animationDown = new Animation();
	animationDown->addFrame(sf::IntRect(3, 3, 17, 29));
	animationDown->addFrame(sf::IntRect(24, 4, 17, 29));
	animationDown->addFrame(sf::IntRect(45, 4, 17, 29));
	animationDown->addFrame(sf::IntRect(66, 4, 17, 29));
	animationDown->addFrame(sf::IntRect(88, 3, 16, 29));
	//Left Walking Animation
	Animation* animationLeft = new Animation();
	animationLeft->addFrame(sf::IntRect(5, 100, 15, 27));
	animationLeft->addFrame(sf::IntRect(25, 99, 16, 28));
	animationLeft->addFrame(sf::IntRect(45, 99, 16, 28));
	animationLeft->addFrame(sf::IntRect(65, 100, 15, 28));
	animationLeft->addFrame(sf::IntRect(87, 99, 15, 28));
	//Right Walking Animation
	Animation* animationRight = new Animation();
	animationRight->addFrame(sf::IntRect(5, 36, 15, 28));
	animationRight->addFrame(sf::IntRect(27, 36, 15, 28));
	animationRight->addFrame(sf::IntRect(47, 36, 15, 28));
	animationRight->addFrame(sf::IntRect(67, 36, 15, 29));
	animationRight->addFrame(sf::IntRect(87, 37, 15, 27));
	//Up Walking Animation
	Animation* animationUp = new Animation();
	animationUp->addFrame(sf::IntRect(4, 70, 17, 25));
	animationUp->addFrame(sf::IntRect(25, 69, 17, 26));
	animationUp->addFrame(sf::IntRect(45, 69, 18, 26));
	animationUp->addFrame(sf::IntRect(66, 69, 17, 26));
	animationUp->addFrame(sf::IntRect(88, 70, 16, 26));
	//Left Idle
	Animation* idleLeft = new Animation();
	idleLeft->addFrame(sf::IntRect(114, 98, 17, 29));
	//Down Idle
	Animation* idleDown = new Animation();
	idleDown->addFrame(sf::IntRect(113, 4, 17, 28));
	//Up Idle
	Animation* idleUp = new Animation();
	idleUp->addFrame(sf::IntRect(113, 70, 17, 25));
	//Right Idle
	Animation* idleRight = new Animation();
	idleRight->addFrame(sf::IntRect(114, 37, 15, 27));

	animationDown->setSpriteSheet(*spriteSheet);
	animationLeft->setSpriteSheet(*spriteSheet);
	animationRight->setSpriteSheet(*spriteSheet);
	animationUp->setSpriteSheet(*spriteSheet);
	idleLeft->setSpriteSheet(*spriteSheet);
	idleDown->setSpriteSheet(*spriteSheet);
	idleRight->setSpriteSheet(*spriteSheet);
	idleUp->setSpriteSheet(*spriteSheet);
	AnimatedSpriteComponent* asc = new AnimatedSpriteComponent();
	asc->mapAnimation("WALK_LEFT", animationLeft);
	asc->mapAnimation("WALK_DOWN", animationDown);
	asc->mapAnimation("WALK_RIGHT", animationRight);
	asc->mapAnimation("WALK_UP", animationUp);
	asc->mapAnimation("IDLE_LEFT", idleLeft);
	asc->mapAnimation("IDLE_DOWN", idleDown);
	asc->mapAnimation("IDLE_RIGHT", idleRight);
	asc->mapAnimation("IDLE_UP", idleUp);
	asc->setAnimation(animationLeft);
	ent->addComponent("animated", asc);
	//END TESTING ANIMATION

	_renderSystem->addEntity(ent);
	_inputSystem->addEntity(ent);
	_movementSystem->addEntity(ent);
}

void GameManager::stop()
{
	_running = false;

	//Free all of our resources
	delete _entityManager;
	delete _renderList;
}

GameManager* Game()
{
	return GameManager::Get();
}

EventManager* GameManager::getEventManager()
{
	return _eventManager;
}

EntityManager* GameManager::getEntityManager()
{
	return _entityManager;
}

ResourceManager* GameManager::getResourceManager()
{
	return _resourceManager;
}

std::vector<Renderable*>* GameManager::getRenderList()
{
	return _renderList;
}