#include "managers\resourcemanager.h"

//Loads the requested texture from the specified source file.
sf::Texture* ResourceManager::loadTexture(const std::string &src)
{
	if (textureList.count(src))
	{
		return textureList[src];
	}
	else
	{
		sf::Texture* texture = new sf::Texture;
		texture->loadFromFile(src);
		textureList[src] = texture;
		return texture;
	}
}