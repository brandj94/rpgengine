/**
* Author: Brandon Jacobsen
* Email: brandj94@vt.edu
* Date Started: 8/9/2015
* Last Update: 8/9/2015
*
**/

#include "managers\scenemanager.h"
#include "managers\gamemanager.h"
#include <algorithm>

//temp sort render list by depth
bool renderSort(Renderable *r1, Renderable *r2)
{
	return r1->getDepth() > r2->getDepth();
}

void SceneManager::update()
{
	/*for (auto & entity : entityList)
		entity->update();*/
}

void SceneManager::render()
{
	/*std::vector<Renderable*> list = Game()->getComponentManager()->getRenderables();
	sortRenderList(&list);
	for (auto & render : list)
	{
		render->render();
	}*/
}

void SceneManager::addEntity(Entity* ent)
{
	entityList.push_back(ent);
}

//temp sort render list by depth
void SceneManager::sortRenderList(std::vector<Renderable*> *list)
{
	std::sort(list->begin(), list->end(), renderSort);
}