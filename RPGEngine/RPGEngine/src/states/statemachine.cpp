#include "states\statemachine.h"

void StateMachine::update()
{
	curState->update();
}

void StateMachine::render()
{
	curState->render();
}

void StateMachine::changeState(const std::string &stateName)
{
	curState->onExit();
	curState = gameStates[stateName];
	curState->onEnter();
}

void StateMachine::addState(const std::string &stateName, IState* state)
{
	gameStates[stateName] = state;
}