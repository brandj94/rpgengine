#include "states\worldmapstate.h"
#include "entities\playerentity.h"
#include "managers\gamemanager.h"

WorldMapState::WorldMapState()
{
	sceneManager = new SceneManager();
	player = new PlayerEntity("res/player.png", sf::Vector2f(100, 100), 0);
	tileMap = new TileMapComponent(64, 10, 5);
	//player->addComponent("renderable", new GraphicComponent("res/player.png", sf::Vector2f(0, 0), 0, true));
	sceneManager->addEntity(player);
}

void WorldMapState::onEnter()
{
	
}

void WorldMapState::onExit()
{

}

void WorldMapState::update()
{
	sceneManager->update();
}

void WorldMapState::render()
{
	sceneManager->render();
}