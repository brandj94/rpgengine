#include "systems\inputsystem.h"
#include "components\inputcomponent.h"
#include "managers\gamemanager.h"

void InputSystem::update()
{
	for (Entity* entity : _entityList)
	{
		std::map<std::string, Component*> components = entity->getComponents();
		if (components["input"] != NULL)
		{
			InputComponent* input = (InputComponent*)components["input"];
			std::vector<int> events = Game()->getEventManager()->requestEvents();
			for (int code : events)
			{
				input->execute(code, entity);
			}
		}
	}
}

void InputSystem::addEntity(Entity* ent)
{
	_entityList.push_back(ent);
}