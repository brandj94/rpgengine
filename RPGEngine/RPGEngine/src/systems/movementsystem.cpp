#include "systems\movementsystem.h"
#include "components\movementcomponent.h"
#include "components\positioncomponent.h"
#include "components\animatedspritecomponent.h"
#include <iostream>

void MovementSystem::update(double dt)
{
	for (Entity * entity : _entityList)
	{
		MovementComponent* move = (MovementComponent*) entity->getComponent("movement");
		PositionComponent* pos = (PositionComponent*) entity->getComponent("position");
		AnimatedSpriteComponent* anim = (AnimatedSpriteComponent*) entity->getComponent("animated");

		if (move->isMoving())
		{
			if (!gotNext)
			{
				curTileX = pos->getPosition().x;
				curTileY = pos->getPosition().y;
				nextTileX = move->getDestinationX();
				nextTileY = move->getDestinationY();
				gotNext = true;
			}

			t += dt * 2;
			int newPosX = lerp(curTileX, nextTileX, t);
			int newPosY = lerp(curTileY, nextTileY, t);
			pos->setPosition(newPosX, newPosY);

			if (t <= 1)
			{
				return;
			}

			std::cout << "stopped moving" << std::endl;
			move->setMoving(false);
			gotNext = false;
			t = 0;

			anim->setFrame(0);

			if (anim->getState() == "WALK_LEFT")
				anim->setState("IDLE_LEFT");
			else if (anim->getState() == "WALK_DOWN")
				anim->setState("IDLE_DOWN");
			else if (anim->getState() == "WALK_RIGHT")
				anim->setState("IDLE_RIGHT");
			else if (anim->getState() == "WALK_UP")
				anim->setState("IDLE_UP");
		}
	}
}

void MovementSystem::addEntity(Entity* ent)
{
	_entityList.push_back(ent);
}

//Linear interpolation
int MovementSystem::lerp(int start, int end, double t)
{
	return start + (end - start) * t;
}