#include "systems\rendersystem.h"
#include "components\graphiccomponent.h"
#include "components\positioncomponent.h"
#include "components\animatedspritecomponent.h"

void RenderSystem::render(double dt)
{
	for (Entity* entity : _entityList)
	{
		std::map<std::string, Component*> components = entity->getComponents();
		if (components["renderable"] != NULL)
		{
			GraphicComponent* graph = (GraphicComponent*)components["renderable"];
			PositionComponent* pos = (PositionComponent*)components["position"];
			sf::Vector2f vecPos = pos->getPosition();
			graph->render(vecPos.x, vecPos.y);
		}
		if (components["animated"] != NULL)
		{
			AnimatedSpriteComponent* sprite = (AnimatedSpriteComponent*)components["animated"];
			PositionComponent* pos = (PositionComponent*)components["position"];
			sf::Vector2f vecPos = pos->getPosition();
			if (sprite->getState() != "")
				sprite->setAnimation(sprite->getState());
			sprite->update(dt);
			sprite->render(vecPos.x, vecPos.y);
		}
	}
}

void RenderSystem::addEntity(Entity* ent)
{
	_entityList.push_back(ent);
}